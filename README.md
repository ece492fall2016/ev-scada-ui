This is where the code for the scada-ui lives as well as the current build that is being hosted by scadad.
The current build being hosted is in build/
You can rebuild it by using webpack

For local testing and debugging the best thing to do is use npm.
```
npm start
```

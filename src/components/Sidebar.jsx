
import React, { Component } from 'react';


const optionStyle = {
  active: {
    background: '#DFDFDF', /* Light grey background */
    fontWeight: 'bold', /* Make sure they're bold */
    border: '1px solid #CCC',
  },

  inActive: {
    background: '#FFFFFF', /* Lighter grey background */
  },
}

class SidebarItem extends Component {

  
  constructor(props){
    super(props);
  }

  handleClick() {
    location.hash = this.props.name;
  }
  render() {
    return <li style={optionStyle['inActive']} onClick={this.handleClick.bind(this)}>
      {this.props.displayName}
    </li>
  }
}

export default class Sidebar extends Component {

  render() {
    let subsystems = this.props.subsystems;
    let virtual = [];
    let physical = [];
    let name = this.props.name;

    Object.keys(subsystems).sort().forEach(function (k) {
      let s = subsystems[k];
      let displayName = ''
      if(k.length > 20){
        displayName = k.replace(/[a-z]/g, '');
      } else {
        displayName = k
      }

      if (s.is_virtual) {
        virtual.push(<SidebarItem name={k} displayName={displayName}/>);
      } else {
        physical.push(<SidebarItem name={k} displayName={displayName}/>);
      }
    });

    return <div style={this.props.style}>
      <h2 onClick={function(){location.hash='';}}>{name}</h2>
      <h4>Virtual</h4>
      <ul>
        {virtual}
      </ul>

      <h4>Physical</h4>
      <ul>
        {physical}
      </ul>
    </div>
  }
}

import React, { Component } from 'react';

import Measurand from './Measurand.jsx'

export default class System extends Component {

  render() {
    let stats = Object.keys(this.props.stats).map(function (e) {
      let m = this.props.stats[e];
      if (m.type == 'measurand') {
        return <p><Measurand {...m}/></p>
      } else {
        return <p><b>{e}</b></p>
      }
    }.bind(this));

    return <div style={this.props.style}>
      <h3>{this.props.name}</h3>
      <p>
        Up since: {this.props.starttime}
      </p>

      <p>
        <b>{this.props.welcome}</b>
      </p>

      <p>
        <b>Events Processed: </b> {this.props.event_count}
      </p>
      <b>Stats:</b>
      {stats}
    </div>
  }
}

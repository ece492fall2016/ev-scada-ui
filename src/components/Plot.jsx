// Copyright 2016 Domenick Falco


/*!
 * Chart.js
 * http://chartjs.org/
 * Version: 1.0.1-beta.4
 *
 * Copyright 2014 Nick Downie
 * Released under the MIT license
 * https://github.com/nnnick/Chart.js/blob/master/LICENSE.md
 */

import React, {Component} from 'react'
// import Controllable, {BooleanControllable} from './Controllable.jsx'

////////////////////////////////////////////////////////////////////////////////
////////////////////////////Variables///////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

var chartIsActive = true;

//fun random colors for charts
const colors = [
    '#FF4500',
    '#4169E1',
    '#FF1493',
    '#00FF00',
    '#FF0000',
    '#00FFFF',
    '#FFD700',
    '#66CDAA',
    '#4B0082',
    '#800000',
]

const componentStyle = {
    borderStyle: 'solid',
    borderWidth: '1px',
}

const boolStyle = {
    left: '1px',
    position: 'relative',
    width: '150px',
    height: '30px',
    fontSize: '16px',
}

//options for the line chart
var chartOptions = {

    // Boolean - Whether to animate the chart
    animation: false,

    //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
    scaleBeginAtZero : false,

    //Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines : true,

    //String - Colour of the grid lines
    scaleGridLineColor : "rgba(0,0,0,.05)",

    //Number - Width of the grid lines
    scaleGridLineWidth : 1,

    //Boolean - Whether the line is curved between points
    bezierCurve : false,

    //Number - Radius of each point dot in pixels
    pointDotRadius : 2,

    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,

    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,

    //Boolean - If there is a stroke on each bar
    barShowStroke : true,

    //Number - Pixel width of the bar stroke
    barStrokeWidth : 2,

    //Number - Spacing between each of the X value sets
    barValueSpacing : 5,

    //Number - Spacing between data sets within X values
    barDatasetSpacing : 1,

    // Boolean - If we want to override with a hard coded scale
    scaleOverride: false,

    // ** Required if scaleOverride is true **
    // Number - The number of steps in a hard coded scale
    scaleSteps: 10,
    // Number - The value jump in the hard coded scale
    scaleStepWidth: 10,
    // Number - The scale starting value
    scaleStartValue: 0,

    // Array - Array of string names to attach tooltip events
    tooltipEvents: ["mousemove", "mouseout", "click", "touchstart", "touchmove", "touchend", "onload", "onfocus"],

    // Number - Size of the caret on the tooltip
	   tooltipCaretSize: 8,

    //adds react
    responsive: false,

    // String - Template string for single tooltips
    	tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",

    // String - Template string for multiple tooltips
    	multiTooltipTemplate: "<%= value %>",

      // Function - Determines whether to execute the customTooltips function instead of drawing the built in tooltips (See [Advanced - External Tooltips](#advanced-usage-external-tooltips))
      customTooltips: false,

    // Boolean - Determines whether to draw tooltips on the canvas or not
      	showTooltips: true,

    //String - A legend template
    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"


}

const style = {
  display: 'inline'
}

const tableStyle = {
  table: {
    color: '#333', /* Lighten up font color */
    fontFamily: 'Helvetica, Arial, sans-serif', /* Nicer font */
    borderCollapse: 'collapse',
    borderSpacing: '0',
    borderStyle: 'solid',
    borderWidth: '3px',
  },

  th: {
    background: '#DFDFDF', /* Light grey background */
    fontWeight: 'bold', /* Make sure they're bold */
    border: '1px solid #CCC',
    height: '30px',
    textAlign: 'center', /* Center our text */
  },

  td_even: {
    background: '#F1F1F1', /* Lighter grey background */
    textAlign: 'center', /* Center our text */
    border: '1px solid #CCC',
    height: '30px',
  },

  td_odd: {
    background: '#FEFEFE', /* Lighter grey background */
    textAlign: 'center', /* Center our text */
    border: '1px solid #CCC',
    height: '30px',
  },
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////Functions///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////




//enables or disables chart data recording
function playPause(){
  chartIsActive = !chartIsActive;
}

//Clears the chart
function clearData(){
    var i=0;
    while (i < timeLabel.length) {
      timeLabel.push(0);
      timeLabel.shift();
      voltageArray.push(0);
      voltageArray.shift();
      i++;
    }
    latestLabel = 0;
}

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

/////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////Components///////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//The react-chartjs chart
var LineChart = require("react-chartjs").Line;

//button to play or pause the graph
var PlayPauseButton = React.createClass({
  getInitialState: function() {
    return {};
  },
  handleClick: function(event) {
    playPause();
  },
  render: function() {
    return (
      <button onClick={this.handleClick} > Play/Pause Graph</button>
    );
  }
});


//button to clear the chart
var ClearButton = React.createClass({
  getInitialState: function() {
    return {};
  },
  handleClick: function(event) {
    clearData();
  },
  render: function() {
    return (
        <button onClick={this.handleClick}> Clear Graph</button>
    );
  }
});

class BooleanControllable extends Component {

  constructor(props){
    super(props);
    this.state = {value: props.input}
  }

  componentWillReceiveProps(props) {
    this.setState({value: props.input});
  }


  handleClick(e){
    let new_value = !Boolean(this.state.value);

    this.setState({value: new_value});
    let ss = {};
    Object.assign(ss, this.props);
    ss.input = new_value;

    api.sendUpdate(ss);
  }


  render() {
    return <div>
      <input type="button"  onClick={this.handleClick.bind(this)} style={boolStyle} value={String(this.state.value)} />
      <span>
      <b> Actual: </b> {String(this.props.actual)}
      </span>
      </div>
  }


}


////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////HTML//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

export default class Plot extends Component {

  constructor(props){
    super(props);
  }

  componentWillReceiveProps(props) {
    //this.forceUpdate();
  }

  render() {
    if(chartIsActive){
      //data for the line chart
     var chartData = {
         labels: this.props.time_axis,


         datasets: [{},{},{},{},{},{},{}],
      };
      

      var legendTable = []
      for (var i = 0; i < this.props.data_sets.length; i++){
        var color = getRandomColor();
        var legendStyle = {
          background: colors[i],
          width: '200px'
        };
        chartData['datasets'][i] =   {
                                      label: this.props.data_watching[i][this.props.data_watching[i].length - 1],
                                      strokeColor: colors[i],
                                      pointColor: colors[i],
                                      pointStrokeColor: "#fff",
                                      pointHighlightFill: "#fff",
                                      pointHighlightStroke: "rgba(0,0,0,1)",
                                      fillColor: "rgba(220,220,220,0)",
                                      data: this.props.data_sets[i]

                                                                                                    }
        legendTable.push(
          <tr>
            <td style={legendStyle}> {this.props.data_watching[i]} </td>
          </tr>)  


    }

    return <div style={componentStyle}>
      <h2> {this.props.location} </h2>
      <span>
        {String(this.props.data_watching[0][this.props.data_watching[0].length - 1])}
        <LineChart data={chartData} options={chartOptions} width="600" height="350"/>
      </span>
      <div><center><b> Time (s) </b></center> </div>
      <div>
        <PlayPauseButton/>
        <span>
          <table style={tableStyle['table']}>
            <tbody>
              {legendTable}
            </tbody>
          </table>
        </span>
        <BooleanControllable {...this.props}/>
      </div>
    </div>
  }
}
}

// Copyright 2016 Brendon Carroll

import React, {Component} from 'react'

class Online extends Component {
  render() {
    let style = {
      background: "#ff0000"
    };
    if (this.props.online) {
      style.background = "#00ff00";
    }
    return <div>
      <b>ONLINE: </b>
      <span style={style}>{this.props.online.toString().toUpperCase()}</span>
    </div>
  }
}

export default class SubsystemHeader extends Component {
    render() {
      let name = this.props.name;
      let online = this.props.online;
      if (this.props.timestamp) {
        var timestamp = Date(this.props.timestamp);
      } else {
        var timestamp = 'Not contacted.'
      }
      return <div>
        <h1>{name}</h1>
        <Online online={this.props.online}/>
        <p><b>Location: </b>{this.props.location}</p>
        <p><b>Timestamp: </b>{timestamp}</p>
      </div>
    }
}

// Copyright 2016 Brendon Carroll

import React, { Component } from 'react';

import SubsystemViews from './subsystem-views'
import System from './System.jsx'
import MeasurandTable from './MeasurandTable.jsx'
import SubsystemHeader from './SubsystemHeader.jsx'

class DefaultSubsystemView extends Component {
  render() {
    return <div>
      <SubsystemHeader {...this.props}/>
      <h4>We couldn't find a view for this subsystem.
      so here is an automatically generated one instead.
      We hope you like it.
      </h4>
      <pre>{JSON.stringify(this.props, null, 4)}</pre>
      <MeasurandTable {...this.props}/>
    </div>
  }
}

export default class MainView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      hash: location.hash.replace('#', '')
    }
  }

  handleHashChange() {
    this.setState({
      hash: location.hash.replace('#', '')
    })
  }

  componentDidMount() {
    window.onhashchange = this.handleHashChange.bind(this);
  }

  componentWillUnmount() {
    window.onhashchange = null;
  }

  render() {
    if (this.state.hash === "") {
      return <System {...this.props}/>
    }
    let subsystem = this.props.subsystems[this.state.hash];
    if (subsystem !== undefined) {
      // Does it have a view?
      var SubsystemView = SubsystemViews[subsystem.systype];
      if (SubsystemView === undefined) {
        SubsystemView = DefaultSubsystemView;
      }
      subsystem.name = this.state.hash
      return <div style={this.props.style}>
        <SubsystemView {...subsystem}/>
      </div>
    }
    return <div style={this.props.style}>
      MainView
    </div>
  }
}

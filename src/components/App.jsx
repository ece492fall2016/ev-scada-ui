
import React, { Component } from 'react';

import MainView from './MainView.jsx'
import Sidebar from './Sidebar.jsx'
import SystemStore from '../stores/system_store.js'

const appStyle = {
  fontFamily: 'Helvetica'
};

const mainViewStyle = {
  border: "1px solid black",
  float: "left",
}

const sidebarStyle = {
  width: '200px',
  float: 'left',
  border: "1px solid black",
}

const upperStyle = {
  position: 'absolute',
  top:0
}

const footerStyle = {
  position: 'absolute',
  bottom: 0,
  textAlign: 'center',
  padding: '5px',
  color: 'lightgrey',
  height: '25px'
}

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      system: SystemStore.getState(),
    };
  }

  handleChange() {
    this.setState({
      system: SystemStore.getState(),
    });
  }

  componentDidMount() {
    SystemStore.on('change', this.handleChange.bind(this));
  }

  componentWillUnmount() {
    SystemStore.removeListener('change', this.handleChange.bind(this));
  }

  render() {
    let system = this.state.system;
    if (system.name === undefined) {
      return <div></div>
    }
    return <div style={appStyle}>
      <div style={upperStyle}>
        <Sidebar style={sidebarStyle} {...system}/>
        <MainView style={mainViewStyle} {...system}/>
      </div>

    </div>
  }
}

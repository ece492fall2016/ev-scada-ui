
import React, { Component } from 'react';
import Controllable from '../Controllable.jsx';

import Measurand from '../Measurand.jsx'
import MeasurandTable from '../MeasurandTable.jsx'
import Plot from '../Plot.jsx'
import getTimeString from '../time_string.js'
import SubsystemHeader from '../SubsystemHeader.jsx'

class Cell extends Component {
  render() {
    let temp = this.props.temperature;
    let voltage = this.props.voltage;
    let num = this.props.num;
    let status = this.props.status;
    return <tr>
        <td>{num}</td>
        <td><Measurand {...voltage}/></td>
        <td><Measurand {...temp}/></td>
        <td><Measurand {...status}/></td>
    </tr>
  }
}

const tableStyle = {
  fontFamily: 'Helvetica'
}

//Table that displays the values recieved
class CellTable extends Component {
  render() {
    let cells=this.props.cells;
    let cellComps = cells.map(function (c) {
      return <Cell {...c}/>
    });
    return <div style={tableStyle}>
      <table>
      <tbody>
        <tr>
          <th>ID</th>
          <th>Voltage</th>
          <th>Temperature</th>
          <th>Status</th>
        </tr>
        {cellComps}
      </tbody>
      </table>
    </div>
  }
}

export default class BatteryPack extends Component {

  render() {
    let cells = this.props.cells;

    return <div>
      <SubsystemHeader {...this.props}/>

      <MeasurandTable {...this.props}/>
      <Plot {...this.props.cell_voltage}/>
      <CellTable cells={cells}/>
    </div>
  }
}

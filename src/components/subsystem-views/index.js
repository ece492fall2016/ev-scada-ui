
import BatteryPack from './BatteryPack.jsx'
import BatteryManager from './BatteryManager.jsx'
import MotorController from './MotorController.jsx'
import Dashboard from './Dashboard'
import LogViewer from './LogViewer.jsx'
import TractiveSystemController from './TractiveSystemController.jsx'
import TractiveSystemManager from './TractiveSystemManager.jsx'
import SafetyManager from './SafetyManager.jsx'
import DynomometerController from './DynomometerController.jsx'

export default {
  'BatteryPack': BatteryPack,
  'BatteryManager': BatteryManager,
  'MotorController': MotorController,
  'Dashboard': Dashboard,
  'LogViewer' : LogViewer,
  'TractiveSystemController' : TractiveSystemController,
  'TractiveSystemManager' : TractiveSystemManager,
  'SafetyManager' : SafetyManager,
  'DynomometerController' : DynomometerController,
}

// Copyright 2016 Domenick Falco

import React, {Component} from 'react'

import Controllable from '../Controllable.jsx'
import MeasurandTable from '../MeasurandTable.jsx'
import SubsystemHeader from '../SubsystemHeader.jsx'
import Measurand from '../Measurand.jsx'



////////////////////////////////////////////////////////////////////////////////
////////////////////////////Variables///////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
const style = {
  display: 'inline'
}


const tableStyle = {
  table: {
    color: '#333', /* Lighten up font color */
    fontFamily: 'Helvetica, Arial, sans-serif', /* Nicer font */
    width: '640px',
    borderCollapse: 'collapse',
    borderSpacing: '0',
    tableLayout: 'fixed',
     wordWrap: 'break-word',
  },

  th: {
    background: '#DFDFDF', /* Light grey background */
    fontWeight: 'bold', /* Make sure they're bold */
    border: '1px solid #CCC',
    height: '30px',
    textAlign: 'center', /* Center our text */
    width: '100px',
  },

  td_even: {
    background: '#F1F1F1', /* Lighter grey background */
    textAlign: 'center', /* Center our text */
    border: '1px solid #CCC',
    height: '100px',

  },

  td_odd: {
    background: '#FEFEFE', /* Lighter grey background */
    textAlign: 'center', /* Center our text */
    border: '1px solid #CCC',
    height: '100px',
  },
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////Functions///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////Components///////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//Table that displays the values recieved
class BatteryTable extends Component {

  constructor(props){
    super(props);
  }

  render() {
    //matrix for table values
    let tableItems=[]

    //find the number of packs
    var numberOfPacks = 0
    for(var m in this.props){
      try{
        if(String(m).charAt(12) > numberOfPacks){

          numberOfPacks = parseInt(String(m).charAt(12))
        }
      }
      catch(err){
      }
    }
    numberOfPacks = numberOfPacks + 1;

    tableItems.push(
      <tr>
        <td style={tableStyle['th']}> Pack Number </td>
        <td style={tableStyle['th']}> Mode </td>
        <td style={tableStyle['th']}> Current </td>
        <td style={tableStyle['th']}> Voltage </td>
        <td style={tableStyle['th']}> State of Charge </td>
        <td style={tableStyle['th']}> Total Coulombs</td>
        <td style={tableStyle['th']}> Uptime</td>
      </tr>)

    for (var i=0; i < numberOfPacks;i++){
      //Need deep copies of the style so it can be changed
      var td = JSON.parse(JSON.stringify(tableStyle['td_odd']));
      if(tableItems.length % 2 == 0){
        td = JSON.parse(JSON.stringify(tableStyle['td_even']));
      }

      let tableStatus = [];

      if(('BatteryPack-' + String(i) + '_mode') in this.props){
        tableStatus.push(<td style={td}>  <Measurand {...this.props['BatteryPack-' + String(i) + '_mode']}/> </td>);
      } 
      else{
        tableStatus.push(<td style={td}> </td>);
      }

      if(('BatteryPack-' + String(i) + '_pack_current') in this.props){
        tableStatus.push(<td style={td}>  <Measurand {...this.props['BatteryPack-' + String(i) + '_pack_current']}/> </td>);
      } 
      else{
        tableStatus.push(<td style={td}> </td>);
      }

      if(('BatteryPack-' + String(i) + '_pack_voltage') in this.props){
        tableStatus.push(<td style={td}>  <Measurand {...this.props['BatteryPack-' + String(i) + '_pack_voltage']}/> </td>);
      } 
      else{
        tableStatus.push(<td style={td}> </td>);
      }

      if(('BatteryPack-' + String(i) + '_state_of_charge') in this.props){
        tableStatus.push(<td style={td}>  <Measurand {...this.props['BatteryPack-' + String(i) + '_state_of_charge']}/> </td>);
      } 
      else{
        tableStatus.push(<td style={td}> </td>);
      }

      if(('BatteryPack-' + String(i) + '_total_coulombs') in this.props){
        tableStatus.push(<td style={td}>  <Measurand {...this.props['BatteryPack-' + String(i) + '_total_coulombs']}/> </td>);
      } 
      else{
        tableStatus.push(<td style={td}> </td>);
      }

      if(('BatteryPack-' + String(i) + '_uptime') in this.props){
        tableStatus.push(<td style={td}>  <Measurand {...this.props['BatteryPack-' + String(i) + '_uptime']}/> </td>);
      } 
      else{
        tableStatus.push(<td style={td}> </td>);
      }

      
      tableItems.push(
        <tr>
          <td style={td}> {String(i)} </td>
          {tableStatus}
        </tr>)

    }

    return <div style={tableStyle}>
      <table style={tableStyle['table']}>
      <tbody>
        {tableItems}
      </tbody>
      </table>
    </div>
  }
}

////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////HTML//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

export default class Battery extends Component {
  render() {

    return <div>
      <SubsystemHeader {...this.props}/>
      <div style={style}>
        <BatteryTable {...this.props}/>
      </div>
    </div>
  }
}

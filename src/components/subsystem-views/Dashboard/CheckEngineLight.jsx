// Copyright 2016 Brendon Carroll

import React, {Component} from 'react';
import ReactDOM from 'react-dom';

const fontSize = 125;
export default class CheckEngineLight extends Component {

  constructor(props) {
    super(props);
    this.state = {flash: false};
  }

  componentWillMount() {
    this.interval = setInterval(function(){
      this.setState({flash: !this.state.flash});
    }.bind(this), 250);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    let y = this.props.y;
    let x = this.props.x;

    let light = null;
    if (this.state.flash) {
      light = <text
        x={x} y={y} fill="white" fontSize={fontSize} textAnchor="middle"
      >
        {String.fromCharCode(9888)}
      </text>;
    }
    let message = null;
    if (this.props.message) {
      message = <text x={x} y={y+fontSize/10} fill="white"
        fontSize={fontSize/17} fontWeight="bolder" textAnchor="middle"
      >
        {this.props.message.toUpperCase()}
      </text>
    }

    return <g>
      {light}
      {message}
    </g>
  }
}

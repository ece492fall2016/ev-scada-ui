
import React, {Component} from 'react';
import ReactDOM from 'react-dom';

let getId = function() {
  let n = 0;
  return function() {
    return 'gauge'+(n++);
  }
}();

const style={

}

class Circle extends Component {
  render() {
    return <circle {...this.props}>
    </circle>
  }
}

const TICK_START = Math.PI * 4 / 3 ;
const TICK_END = Math.PI * 5 / 3;
const NUM_TICKS = 10;
const TICK_INTERVAL = (2*Math.PI - (TICK_END - TICK_START)) / NUM_TICKS;

let fontSize = 30;

export default class Gauge extends Component {
  componentWillMount() {
    this.id = getId();
  }
  render() {
    let x = this.props.x;
    let y = this.props.y;
    let r = this.props.r;
    let maxValue = this.props.maxValue;
    let minValue = this.props.minValue;
    let data = this.props.data;
    let fontSize = r * 30/200;

    // Generate Ticks
    let tickStuff = [];
    for (let i=0; i<=NUM_TICKS; i++) {
      let cos = Math.cos(TICK_START - i * TICK_INTERVAL);
      let sin = Math.sin(TICK_START - i * TICK_INTERVAL);
      let t = <text
        x={x + r*4/5*cos}
        y={y - r*4/5*sin}
        fill="white"
        textAnchor="middle"
        fontSize={fontSize*4/7}
        fontWeight="bold"
      >
        {minValue + maxValue * i / NUM_TICKS}
      </text>
      let l = <line
        x1={x + r*15/20*cos} x2={x + r*cos}
        y1={y - r*15/20*sin} y2={y - r*cos}
        stroke="white"
        strokeWidth="2"
      />
      tickStuff.push(t);
      //tickStuff.push(l)
    }

    let mask2Ticks = [];
    for (let i=0; i<=(NUM_TICKS*7)*((data-minValue)/(maxValue - minValue)*6/5); i++) {
      let tickWidth = Math.PI / 120;
      let r1 = TICK_START - i *TICK_INTERVAL/7 + tickWidth;
      let r2 = TICK_START - i *TICK_INTERVAL/7 - tickWidth;
      mask2Ticks.push(
        <polygon points={[
          [x, y],
          [x + r*Math.cos(r1), y-r*Math.sin(r1)],
          [x + r*Math.cos(r2), y-r*Math.sin(r2)]
        ]}
        fill="white"
        />
      );
    }

    let mask1 = <mask id={this.id+"mask1"}>
      <Circle cx={x} cy={y} r={r} fill="white"/>
      <polygon points={[
        [x,y],
        [x+2*r*Math.cos(TICK_START), y-2*r*Math.sin(TICK_START)],
        [x+2*r*Math.cos(TICK_END), y-2*r*Math.sin(TICK_END)]]}
      fill="black"/>
    </mask>

    let mask2 = <mask id={this.id+"mask2"}>
      <Circle cx={x} cy={y} r={r} fill="black"/>
      {mask2Ticks}
    </mask>

    return <g>
    <defs>
      {mask1}
      {mask2}
    </defs>
    <g mask={"url(#"+this.id+"mask1)"}>
    <Circle
        cx={x}
        cy={y}
        r={r}
        stroke="white"
        strokeWidth={8/200*r}
      />
    <Circle
      cx={x}
      cy={y}
      r={r-16/200*r}
      strokeWidth={8/200*r}
      stroke="white"
      mask={"url(#"+this.id+"mask2)"}
    />
    </g>

    <text x={x}
      y={y+r/3} fill="white"
      fontSize={fontSize}
      textAnchor='middle'>
      {this.props.title}
    </text>

    <text x={x} y={y+r/7} fill="white" textAnchor="middle" fontSize={fontSize*3}>
      {Math.round(this.props.data).toFixed()}
    </text>
    {tickStuff}
    </g>;
  }
}

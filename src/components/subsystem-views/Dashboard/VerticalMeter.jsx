// Copyright 2016 Brendon Carroll

import React, {Component} from 'react'

let height = 100;
let width = 30;

export default class VerticalMeter extends Component {
  render() {
    let value = this.props.value;
    let max = this.props.max;

    let bottom = this.props.y + height;
    return <g>
      <text x={this.props.x} y={this.props.y}
      fill="white" textAnchor="middle">
        {this.props.title}
      </text>
      <rect x={this.props.x-width/2} y={this.props.y + 7} width={width} height={height}
      stroke="white" strokeWidth="1"/>
      <rect x={this.props.x-width/2} y={bottom-(height)*value/max+7} height={(height)*value/max} width={width}
      fill="white"/>
    </g>
  }
}

// Copyright 2016 Brendon Carroll

import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class GearIndicator extends Component {
  render() {
    let data = this.props.data
    let x = this.props.x;
    let y = this.props.y;
    let size = 50;

    return <g>
      <rect x={x-size/2} y={y-size/2}
        width={size} height={size}
        fill="white" stroke="white"/>
      <text x={x} y={y+size/2 - 10} fontSize="50"
        textAnchor="middle"
        fontWeight="800"
      >
        {this.props.value}
      </text>
    </g>
  }
}

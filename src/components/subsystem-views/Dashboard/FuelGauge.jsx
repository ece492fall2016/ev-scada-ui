// Copyright 2016 Brendon Carroll

import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class FuelGauge extends Component {

  render() {
    let x = this.props.x;
    let y = this.props.y;
    let r = this.props.r;

    let pivotX = x;
    let pivotY = y+r;
    let pointerLength = r * (1/Math.cos(Math.PI/6) + Math.tan(Math.PI/6));
    let pointerPhi = + Math.PI * 2/3 - this.props.data * Math.PI*1/3;

    return <g>
      <circle cx={x} cy={y} r={r} stroke="white" strokeWidth={r*3/100}/>
      <line
        x1={pivotX} y1={pivotY}
        x2={pivotX + pointerLength*Math.cos(pointerPhi)}
        y2={pivotY - pointerLength*Math.sin(pointerPhi)}
        stroke="white" strokeWidth={r*4/100}/>

      <text x={x-r*3/4} y={y}
        fontSize={40/200*r} fill="white" fontWeight="bold" textAnchor="middle">
        E
      </text>

      <text x={x+r*3/4} y={y}
        fontSize={40/200*r} fill="white" fontWeight="bold" textAnchor="middle">
        F
      </text>
      <text x={x} y={y-r/2}
        fontSize={60/200*r} fill="white" fontWeight="bold" textAnchor="middle">
        {(this.props.data*100).toFixed()}%
      </text>
    </g>
  }
}

// Copyright 2016 Brendon Carroll
"use strict";

import React, {Component} from 'react';
import ReactDOM from 'react-dom'

import Controllable from '../../Controllable.jsx'


import Gauge from './Gauge.jsx'
import VerticalMeter from './VerticalMeter.jsx'
import FuelGauge from './FuelGauge.jsx'
import CheckEngineLight from './CheckEngineLight.jsx'
import GearIndicator from './GearIndicator.jsx'



const buttonStyle = {
    borderStyle: 'solid',
    borderWidth: '1px',
}

const width = 780;
const height = width*5.8/16;
const smallR = 50;
const medR = 80;
const gaugeR = 120;


const style = {
  background: 'black',
  fontFamily: 'Lucida Sans Unicode, sans-serif',
  color: 'white',
  width: width,
}

export default class Dashboard extends Component {



  render() {
    
    try{
       document.getElementById('dash').scrollIntoView();
       

    }
    catch(err) {
       console.log("welcome to the dashboard")
    }

    return <div id='dash' style={style}>
    <center  onClick={function(){location.hash='';}}><b style={buttonStyle}> Click Me for Maintenence Mode</b></center>
    <svg id='dash' width={width} height={height}>
        <Gauge
          title="CURRENT (A)"
          data={this.props.current}
          x={width*1/4} y={height*6.2/10}
          r={gaugeR}
          maxValue={800}
          minValue={-100}
        />
        <Gauge
          title="SPEED (mph)"
          data={this.props.speed}
          x={width*3/4} y={height*6.4/10}
          r={gaugeR}
          minValue={0}
          maxValue={100}
        />
        <Gauge
          title="MOTOR TEMP"
          data={this.props.motor_temperature}
          x={width*1/2} y={height * 2.5/10}
          r={medR}
          minValue={0}
          maxValue={200}
        />
        <FuelGauge
          title="FUEL"
          data={this.props.fuel}
          x={smallR + 10} y={smallR + 10}
          r={smallR}
        />
        <CheckEngineLight
          x={width-60} y={height*3.1/10}
          message="Error goes here"
        />
        <GearIndicator
          x={width/2} y={height*9/10}
          value={this.props.gear}
        />
        
    </svg>
    <span>
    <table>
      <tr>
        <td><Controllable {...this.props.software_throttle}/></td>
        <td><Controllable {...this.props.valve_position}/></td>
      </tr>
      <tr>
        <td><Controllable {...this.props.throttle_enable}/></td>
        <td><Controllable {...this.props.throttle_select}/></td>
      </tr>
    </table>
    </span>
    </div>
  }
}

// let dashboard = {
//   speed: 30.01,
//   current: 200,
//   temp: 30,
// };
//
// let appNode = document.getElementById('app');
//
// setInterval(function () {
//   dashboard.speed += Math.round(Math.random()*2 - 1);
//   dashboard.current = dashboard.speed*10;
//   ReactDOM.render(<Dashboard {...dashboard}/>, appNode);
// }, 100);
//
// console.log('test');

// Copyright 2016 Domenick Falco

import React, {Component} from 'react'

import Controllable from '../Controllable.jsx'
import MeasurandTable from '../MeasurandTable.jsx'
import SubsystemHeader from '../SubsystemHeader.jsx'
import Measurand from '../Measurand.jsx'



////////////////////////////////////////////////////////////////////////////////
////////////////////////////Variables///////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
const style = {
  display: 'inline'
}


const tableStyle = {
  table: {
    color: '#333', /* Lighten up font color */
    fontFamily: 'Helvetica, Arial, sans-serif', /* Nicer font */
    width: '640px',
    borderCollapse: 'collapse',
    borderSpacing: '0',
    tableLayout: 'fixed',
     wordWrap: 'break-word',
  },

  th: {
    background: '#DFDFDF', /* Light grey background */
    fontWeight: 'bold', /* Make sure they're bold */
    border: '1px solid #CCC',
    height: '30px',
    textAlign: 'center', /* Center our text */
    width: '100px',
  },

  td_even: {
    background: '#F1F1F1', /* Lighter grey background */
    textAlign: 'center', /* Center our text */
    border: '1px solid #CCC',
    height: '100px',

  },

  td_odd: {
    background: '#FEFEFE', /* Lighter grey background */
    textAlign: 'center', /* Center our text */
    border: '1px solid #CCC',
    height: '100px',
  },
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////Functions///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////Components///////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//Table that displays the values recieved
class SafetyTable extends Component {

  constructor(props){
    super(props);
  }

  render() {
    //matrix for table values
    let tableItems=[]
    var propsList = Object.keys(this.props.safety_dict)
    propsList.sort()

    tableItems.push(
      <tr>
        <td style={tableStyle['th']}> Measurand Name </td>
        <td style={tableStyle['th']}> Value </td>
        <td style={tableStyle['th']}> Warning </td>
        <td style={tableStyle['th']}> Warning Enabled </td>
        <td style={tableStyle['th']}> Failure </td>
        <td style={tableStyle['th']}> Failure Enabled</td>
        <td style={tableStyle['th']}> Reset Failure</td>
      </tr>)

    for (var i in propsList){
      let m = propsList[i];
      //Need deep copies of the style so it can be changed
      var td = JSON.parse(JSON.stringify(tableStyle['td_odd']));
      if(tableItems.length % 2 == 0){
        td = JSON.parse(JSON.stringify(tableStyle['td_even']));
      }

      let tableStatus = [];



      if((m + '_warning') in this.props){
        if(this.props[m + '_warning'].calibrated){
          td['background'] = '#FFFF00'
        }
        tableStatus.push(<td style={td}> {this.props.safety_dict[m]['warning']} {String(this.props[m + '_warning'].calibrated)} </td>);
        tableStatus.push(<td style={td}> <Controllable {...this.props[m + '_warning_toggle']}/></td>);
      } 
      else{
        tableStatus.push(<td style={td}> </td>);
        tableStatus.push(<td style={td}> </td>);
      }

      if((m + '_failure') in this.props){
        if(this.props[m + '_failure'].calibrated){
          td['background'] = '#FF0000'
        }
        tableStatus.push(<td style={td}> {this.props.safety_dict[m]['failure']} {String(this.props[m + '_failure'].calibrated)} </td>);
        tableStatus.push(<td style={td}> <Controllable {...this.props[m + '_failure_toggle']}/></td>);
      } 
      else{
        tableStatus.push(<td style={td}> </td>);
        tableStatus.push(<td style={td}> </td>);
      }

      if((m + '_reset') in this.props){
        tableStatus.push(<td style={td}> <Controllable {...this.props[m + '_reset']}/></td>);
      } 
      else{
        tableStatus.push(<td style={td}> </td>);
      }

      let inputType = typeof this.props[m];
      if(inputType = 'object' && m != __proto__ && m != null && m!= 'name' && m!= 'online' && m!= 'is_virtual' && m != 'type' && this.props[m].type == 'measurand'){
        tableItems.push(
          <tr>
            <td style={td}> {String(m).replace("_", " ").replace("_", " ")} </td>
            <td style={td}><Measurand {...this.props[m]}/> </td>
            {tableStatus}
          </tr>)
      }

    }

    return <div style={tableStyle}>
      <table style={tableStyle['table']}>
      <tbody>
        {tableItems}
      </tbody>
      </table>
    </div>
  }
}

////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////HTML//////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

export default class SafetyManager extends Component {
  render() {

    return <div>
      <SubsystemHeader {...this.props}/>
      <div style={style}>
        <SafetyTable {...this.props}/>
      </div>
    </div>
  }
}

// Copyright 2016 Brendon Carroll and Domenick Falco
import React, {Component} from 'react'
import Controllable from '../Controllable.jsx'
import MeasurandTable from '../MeasurandTable.jsx'
import SubsystemHeader from '../SubsystemHeader.jsx'

import Plot from '../Plot.jsx'

export default class MotorController extends Component {
  render() {
    let name = this.props.name;
    let location = this.props.location;
   return <div>
    <SubsystemHeader {...this.props}/>
    <MeasurandTable {...this.props}/>
   </div>
  }
}

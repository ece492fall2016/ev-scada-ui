// Copyright 2016 Domenick Falco

import React, {Component} from 'react'

import Measurand from './Measurand.jsx'

////////////////////////////////////////////////////////////////////////////////
////////////////////////////Variables///////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
const style = {
  display: 'inline'
}

const tableStyle = {
  table: {
    color: '#333', /* Lighten up font color */
    fontFamily: 'Helvetica, Arial, sans-serif', /* Nicer font */
    width: '640px',
    borderCollapse: 'collapse',
    borderSpacing: '0'
  },

  th: {
    background: '#DFDFDF', /* Light grey background */
    fontWeight: 'bold', /* Make sure they're bold */
    border: '1px solid #CCC',
    height: '30px',
    textAlign: 'center', /* Center our text */
  },

  td_even: {
    background: '#F1F1F1', /* Lighter grey background */
    textAlign: 'center', /* Center our text */
    border: '1px solid #CCC',
    height: '30px',
  },

  td_odd: {
    background: '#FEFEFE', /* Lighter grey background */
    textAlign: 'center', /* Center our text */
    border: '1px solid #CCC',
    height: '30px',
  },
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////Functions///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////Components///////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//Table that displays the values recieved
export default class MeasurandTable extends Component {

  constructor(props){
    super(props);
  }

  render() {
    //matrix for table values
    let tableItems=[]
    var propsList = Object.keys(this.props)
    propsList.sort()

    tableItems.push(
      <tr>
        <td style={tableStyle['th']}> Measurand Name </td>
        <td style={tableStyle['th']}> Value </td>
      </tr>)

    for (var i in propsList){
      let m = propsList[i];
      var td = tableStyle['td_odd'];
      if(tableItems.length % 2 == 0){
        td = tableStyle['td_even'];
      }
      let inputType = typeof this.props[m];
      if(inputType = 'object' && m != __proto__ && m != null && m!= 'name' && m!= 'online' && m!= 'is_virtual' && m != 'type' && this.props[m].type == 'measurand'){
        tableItems.push(
          <tr>
            <td style={td}> {String(m)} </td>
            <td style={td}><Measurand {...this.props[m]}/> </td>
          </tr>)
      }

    }

    return <div style={tableStyle}>
      <table style={tableStyle['table']}>
      <tbody>
        {tableItems}
      </tbody>
      </table>
    </div>
  }
}

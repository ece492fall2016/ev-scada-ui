// Copyright 2016 Brendon Carroll and Domenick Falco

import React, {Component} from 'react'

import getTimeString from './time_string.js'

// Returns number as an engineering formatted string.
function getEngNumber(number) {
  if (number === 0) {
    return String(0) + ' '
  }
  let engPrefix = new Map([
    [-15, 'f'],
    [-12, 'p'],
    [-9, 'n'],
    [-6, 'u'],
    [-3, 'm'],
    [ 0, ''],
    [ 3, 'K'],
    [ 6, 'M'],
    [ 9, 'G'],
    [12, 'T'],
    [15, 'P']
  ]);
  let power = Math.floor(Math.log10(Math.abs(number)));
  let engPower = Math.floor(power / 3) * 3;
  let prefix = engPrefix.get(engPower);

  number = number / Math.pow(10, engPower);
  return number.toFixed(6) + ' ' + prefix;
}

const ucalStyle = {
  color: 'lightgrey'
}

const calStyle = {

}

export default class Measurand extends Component {
  render() {
    let ucal = this.props.uncalibrated;
    let cal = this.props.calibrated;
    let unit = this.props.unit;
    let precision = this.props.precision;
    if (precision === undefined) {
      precision = 1000;
    }

    // Seconds
    if (unit === 's') {
      cal = getTimeString(cal);
    }
    // Numbers
    else if ((typeof cal === 'number') && !(unit == '%')) {
      cal = getEngNumber(cal, precision) + unit;
    }
    else {
      cal = String(cal) + ' ' + unit;
    }

    return <span>
      <span style={calStyle}>{cal}</span>
      <span style={ucalStyle}> ({String(ucal)})</span>
    </span>
  }
}

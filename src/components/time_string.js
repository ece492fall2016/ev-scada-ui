
function formatNumber(n, places) {
  let nString = n.toString();
  let missingZeros = Math.floor(places - nString.length);
  if (missingZeros > 0) {
    return '0'.repeat(missingZeros) + nString;
  } else if (missingZeros < 0) {
    return nString.slice(-places);
  } else {
    return nString;
  }
}

export default function getTimeString(time) {
  let s = Math.floor(time) % 60;
  let ms = (time - s) * 1000;
  let min = Math.floor(time / 60) % 60;
  let hr = Math.floor(time / (60*60)) % 24;
  let d = Math.floor(time / (60*60*24));

  return [
    d, ':',
    formatNumber(hr, 2), ':',
    formatNumber(min, 2), ':',
    formatNumber(s, 2), '.',
    formatNumber(ms, 3)
  ].join('');
}

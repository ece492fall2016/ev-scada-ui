// Copyright 2016 Domenick Falco

import React, {Component} from 'react'
import api from '../api.js'

const buttonStyle = {
    left: '1px',
    top: '-8px',
    position: 'relative',
    width: '25px',
    height: '25px',
}

const sliderStyle = {
    width: '200px',
    height: '25px',
}

const boolStyle = {
    left: '1px',
    width: '70px',
    height: '30px',
    fontSize: '16px',
}


const componentStyle = {
    borderStyle: 'solid',
    borderWidth: '1px',
}


//A silder controllable component
class RangeControllable extends Component {

  constructor(props){
    super(props);
    this.state = {value: props.input, changing: false}
  }
  startChanging() {
    this.setState({changing: true});
  }

  endChanging() {
    this.setState({changing: false});
    this.setState({value: this.props.input});
  }

  handleChange(e) {
    this.startChanging();
    let new_value = Number(e.target.value);
    this.setState({value: new_value});
    this.endChanging();

    let ss = {};
    Object.assign(ss, this.props);
    ss.input = new_value;
    api.sendUpdate(ss);
  }

  subtractStep(e){
    let min_value = Number(this.props.min)
    let new_value = Number(this.state.value) - Number(this.props.step);
    if(new_value >= min_value){
      this.setState({value: new_value});
      let ss = {};
      Object.assign(ss, this.props);
      ss.input = new_value;
      api.sendUpdate(ss);
    }
    else{
      this.setState({value: min_value});
      let ss = {};
      Object.assign(ss, this.props);
      ss.input = new_value;
      api.sendUpdate(ss);
    }
  }


  additionStep(e){
    let max_value = Number(this.props.max)
    let new_value = Number(this.state.value) + Number(this.props.step);
    if(new_value <= max_value){
      this.setState({value: new_value});
      let ss = {};
      Object.assign(ss, this.props);
      ss.input = new_value;
      api.sendUpdate(ss);
    }
    else{
      this.setState({value: max_value});
      let ss = {};
      Object.assign(ss, this.props);
      ss.input = new_value;
      api.sendUpdate(ss);
    }
  }

  componentWillReceiveProps(props) {
    this.setState({value: this.props.input});

  }

  render() {
    return <div>
      <button onClick={this.subtractStep.bind(this)} style={buttonStyle}> - </button>
      <input type="range"
        min={this.props.min}
        step={this.props.step}
        max={this.props.max}
        value={this.state.value}
        onInput={this.handleChange.bind(this)}
        style={sliderStyle}
      />
      <button onClick={this.additionStep.bind(this)} style={buttonStyle}> + </button>
      <span>
        <b> Actual: </b> {this.props.actual}
      </span>
      <div>

      <b> Input: </b> {this.state.value}
      </div>
      </div>
  }
}


class BooleanControllable extends Component {

  constructor(props){
    super(props);
    this.state = {value: props.input}
  }

  componentWillReceiveProps(props) {
    this.setState({value: props.input});
  }


  handleClick(e){
    let new_value = !Boolean(this.state.value);

    this.setState({value: new_value});
    let ss = {};
    Object.assign(ss, this.props);
    ss.input = new_value;

    api.sendUpdate(ss);
  }


  render() {
    return <div>
      <input type="button"  onClick={this.handleClick.bind(this)} style={boolStyle} value={String(this.state.value)} />
      <span>
      <b> Actual: </b> {String(this.props.actual)}
      </span>
      </div>
  }


}

class ListControllable extends Component {

  constructor(props){
    super(props);
    this.state = {value: props.input}
  }

  componentWillReceiveProps(props) {
    this.setState({value: props.input});
  }

  handleClick(e){
    let new_value = e.target.value;
    this.setState({value: new_value});

    let ss = {};
    Object.assign(ss, this.props);
    ss.input = new_value;
    api.sendUpdate(ss);
    this.forceUpdate();
  }

  render() {
    var options = [];
    for (let i = 0; i < this.props.possibilities.length; i++) {
      options.push (<option value={this.props.possibilities[i]}> {String(this.props.possibilities[i])} </option>);
    }
    return <div>
      <select id="mySelect" value={String(this.state.value)} onChange={this.handleClick.bind(this)}>
          {options}
      </select>
      <span>
      <b> Actual: </b> {String(this.props.actual)}
      </span>
      </div>
  }
}


export default class Controllable extends Component {

  constructor(props){
    super(props);
    this.state = {value: props.input, changing: false}
  }

  componentWillReceiveProps(props) {
    this.forceUpdate();
  }

  render() {
    let inputType = typeof this.props.input;
    var name = 'name not found'
    try{
      name = this.props.location[this.props.location.length - 1];
    }
    catch(err){
    }
    let c = null;
    switch(inputType){
      case 'number':
        c =  <RangeControllable {...this.props}/>
        break;
      case 'boolean':
        c = <BooleanControllable{...this.props}/>
        break;
      case 'string':
        c = <ListControllable{...this.props}/>
        break;
      default:
        c = <div> You've configured this controllable incorrectly </div>
        break;
      }
      return <div style={componentStyle}>
        <b>{String(name).replace("_", " ").replace("_", " ").replace("_", " ").replace("_", " ").replace("_", " ").replace("_", " ")}</b>
        {c}
      </div>
  }
}

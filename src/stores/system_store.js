// Copyright 2016 Brendon Carroll

import EventEmitter from 'events';

function copy_keys(src, dest) {
  Object.keys(src).forEach(function (k) {
    if (typeof dest[k] === "object" && dest[k] !== null) {
      copy_keys(src[k], dest[k]);
    } else {
      dest[k] = src[k];
    }
  });
}

function replaceByLocation(root, i, newObj) {
  let location = newObj.location;
  if (i === location.length - 1) {
    root[location[i]] = newObj;
  } else {
    replaceByLocation(root[location[i]], i+1, newObj);
  }

}

class SystemStore extends EventEmitter{
  constructor() {
    super();
    this.system = {};
    this.message_count = 0;
  }

  update(msg) {
    let ss = null;
    switch (msg.type) {
    case "system":
      copy_keys(msg, this.system);
      break;

    case "subsystem":
      let subsystem = this.system.subsystems[msg.name];
      copy_keys(msg, subsystem);
      break;

    case "measurand":
      ss = this.system.subsystems[msg.location[0]];
      replaceByLocation(ss, 1, msg);
      break;

    case "controllable":
      ss = this.system.subsystems[msg.location[0]];
      replaceByLocation(ss, 1, msg);
      break;

    case "plot":
     // stuff
      break;
    }
    this.message_count += 1;
    this.emit('change');
  }

  getState() {
    return this.system;
  }


}

let ss = new SystemStore();

export default ss;

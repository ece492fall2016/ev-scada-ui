// Copyright 2016 Brendon Carroll

import EventEmitter from 'events'
import SystemStore from './stores/system_store.js'

let hostname = location.hostname;
//host = '139.147.103.157';

let ws = new WebSocket('ws://'+ hostname + ':1428' + '/ws');

if (hostname !== '127.0.0.1') {
  let password = prompt("What's the password?");
  let auth = {
    type: 'auth',
    password: password,
  }
  ws.send(JSON.stringify(auth));
}

let api = {};

ws.onmessage = function (e) {
  let msg = JSON.parse(e.data);
  SystemStore.update(msg);
}

api.sendUpdate = function (newobj) {
  ws.send(JSON.stringify(newobj));
}

export default api;

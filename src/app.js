'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App.jsx';
import api from './api.js'

let appNode = document.getElementById('app');

ReactDOM.render(<App/>, appNode);
